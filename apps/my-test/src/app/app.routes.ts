import { Route } from '@angular/router';

export const appRoutes: Route[] = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        loadComponent: () => 
            import('@angular-monorepo/dashboard').then((m) => m.DashboardComponent),
    },
    {
        path: 'chart',
        loadComponent: () => 
            import('@angular-monorepo/chart').then((m) => m.ChartComponent)
    },
    {
        path: '**',
        loadComponent: () => 
            import('@angular-monorepo/wrongpage').then((m) => m.WrongpageComponent)
    }
];
