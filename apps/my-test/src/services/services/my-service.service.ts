import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  apiUrl = 'http://localhost:4200/'

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(
    private httpClient: HttpClient
  ) { }

  getListPosts(): Observable<any> {
    return this.httpClient
      .get<any>(this.apiUrl + 'posts')
      .pipe(
        // 
      )
  }
}
