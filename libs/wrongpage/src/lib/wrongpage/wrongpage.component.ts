import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'angular-monorepo-wrongpage',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './wrongpage.component.html',
  styleUrl: './wrongpage.component.css',
})
export class WrongpageComponent {}
