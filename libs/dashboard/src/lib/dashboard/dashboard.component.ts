import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MyServiceService } from '../services/services/my-service.service';
import { Subject, takeUntil } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CreateDataComponent } from '../create-data/create-data.component';

@Component({
  selector: 'angular-monorepo-dashboard',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css',
})
export class DashboardComponent implements OnInit, AfterViewInit {
  componentDestroyed$: Subject<boolean> = new Subject();
  displayedColumns: string[] = ['id', 'name', 'action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort();

  constructor(
    private configService: MyServiceService,
    public dialog: MatDialog
  ) {}
  ngOnInit(): void {
    this.getPosts();
  }

  ngAfterViewInit() {
    // 
  }

  getPosts() {
    this.configService
      .getListPosts()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((res: any) => {
        // console.log('res -> ', res);
        const dataPosts = res.map((data: any) => {
          return {
            id: data.id,
            name: data.title,
            body: data.body
          };
        });

        this.displayedColumns = ['id', 'name', 'action'];
        this.dataSource = new MatTableDataSource<any>(dataPosts);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  callbackBtn(obj: { status: string; element: any }) {
    console.log('obj -> ', obj);

    if (obj.status === 'new') {
      const dialogRef = this.dialog.open(CreateDataComponent, {
        width: '50%',
        data: obj,
      });
    } else if (obj.status === 'delete') {
      this.configService.deletePosts(obj.element.id)
      .pipe(takeUntil(this.componentDestroyed$)).subscribe((res => {
        console.log('delete post -> ', res);
      }))
    } else if (obj.status === 'edit') {
      const dialogRef = this.dialog.open(CreateDataComponent, {
        width: '50%',
        data: obj
      })
    }
  }
}
