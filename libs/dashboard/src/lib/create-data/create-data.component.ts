import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { MyServiceService } from '../services/services/my-service.service';
import { Subject, takeUntil } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'angular-monorepo-create-data',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
  ],
  templateUrl: './create-data.component.html',
  styleUrl: './create-data.component.css',
})
export class CreateDataComponent implements OnInit {
  addDataForm: FormGroup;
  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private configService: MyServiceService,
    public dialogRef: MatDialogRef<CreateDataComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.addDataForm = this.fb.group({
      title: ['', [Validators.required]],
      body: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    console.log('data -> ', this.data);
    if(this.data.status === 'edit'){
      this.patchData();
    }
  }

  onSubmit() {
    if (this.data.status === 'new') {
      this.configService
      .submitPosts(this.addDataForm.value.title, this.addDataForm.value.body, '')
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((res) => {
        console.log('createPosts -> ', res);
        this.dialogRef.close(res);
      });
    } else {
      this.configService.updatePosts(this.addDataForm.value.title, this.addDataForm.value.body, this.data.element.id, this.data.element.id)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((res) => {
        console.log('updatePosts -> ', res);
        this.dialogRef.close(res);
      })
    }

      
  }

  patchData(){
    this.addDataForm.patchValue({
      title: this.data.element.name,
      body: this.data.element.body
    })
  }
}
