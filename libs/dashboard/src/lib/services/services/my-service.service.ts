import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

// https://jsonplaceholder.typicode.com/posts

export class MyServiceService {
  apiUrl = 'https://jsonplaceholder.typicode.com';

  httpOptionsLogin = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private httpClient: HttpClient,
  ) { }

  getListPosts(): Observable<any> {
    return this.httpClient
      .get<any>(this.apiUrl + '/posts')
      .pipe()
  }

  // submitPosts(): Observable<any> {
  //   return this.httpClient
  //     .post<any>(this.apiUrl + '/posts', {
  //       //
  //     })
  //     .pipe()
  // }

  submitPosts(title: string, body: string, userId: string): Observable<any> {
    return this.httpClient
      .post<any>(this.apiUrl + '/posts', {
        'title': title,
        'body': body,
        'userId': userId
      })
      .pipe()
  }

  deletePosts(id: string): Observable<any> {
    return this.httpClient
      .delete<any>(this.apiUrl + '/posts/' + id)
    .pipe()
  }

  updatePosts(title: string, body: string, userId: string, id:string): Observable<any> {
    return this.httpClient
      .put<any>(this.apiUrl + '/posts/' + id, {
        'title': title,
        'body': body,
        'userId': userId,
        'id': id
      })
    .pipe()
  }
}
